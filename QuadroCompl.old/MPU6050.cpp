#include "MPU6050.h"

#define RESTRICT_PITCH // Comment out to restrict roll to ±90deg instead
char regaddr[2];

IMU::IMU(void)
{	 
	CFData.x=0;
	CFData.y=0;


	char buf[1];
	bcm2835_init();
	bcm2835_i2c_begin();
	bcm2835_i2c_setSlaveAddress(MPU6050_ADDRESS);
	WriteRegister(SMPRT_DIV,0x07);	// Set the sample rate to 1000Hz - 8kHz/(7+1) = 1000Hz
	WriteRegister(CONFIG,0x00); // Disable FSYNC and set 260 Hz Acc filtering, 256 Hz Gyro filtering, 8 KHz sampling
	WriteRegister(GYRO_CONFIG,0x00); //250dpi
	WriteRegister(ACCEL_CONFIG,0x00); //2g resolution
	WriteRegister(PWR_MGMT_1,0x00); //sleep mode disabled

	
	regaddr[0]=WHO_AM_I;
	bcm2835_i2c_write(regaddr, 1);
	bcm2835_i2c_read(buf, 1);
	if(buf[0]==0x71)
	{
		printf("sensor config was successful WHO_AM_I: %x\n",buf[0]);
	}
	else
	{
		printf("sensor config was unsuccessful, %x\n",buf[0]);
	}
	bcm2835_i2c_end();
}

void IMU::ReadGyr()
{
	//printf("G:");
	GData.x=ReadRegisterPair(GYRO_XOUT_H);//-100;//-448.6;
	GData.y=ReadRegisterPair(GYRO_YOUT_H);//-2541;
	GData.z=ReadRegisterPair(GYRO_ZOUT_H);//-181;
	/*if(GData.x < 500 && GData.x > -500 || GData.x > 65000)
	{
		GData.x=0;	
	}
	if(GData.y < 500 && GData.y > -500 || GData.y > 65000)
	{
		GData.y=0;	
	}
	if(GData.z < 500 && GData.z > -500 || GData.z > 65000)
	{
		GData.z=0;	
	}*/
	printf("Gyr: %f %f %f\n",GData.x,GData.y,GData.z);
	return;

}

void IMU::ReadAccel()
{
	//printf("A:");
	AData.x=ReadRegisterPair(ACCEL_XOUT_H);//+17220;
	AData.y=ReadRegisterPair(ACCEL_YOUT_H);//+385;
	AData.z=ReadRegisterPair(ACCEL_ZOUT_H);//+17220;
	
	printf("Acc: %f %f %f\n",AData.x,AData.y,AData.z);
	return;

}

void IMU::PrintDatas()
{

	printf("Gyr: %f %f %f Acc: %f %f %f CFilter: %f %f\n",GData.x,GData.y,GData.z,AData.x,AData.y,AData.z,CFData.x,CFData.y);

}

int ReadRegisterPair(int REG_H)
{
	char buf[1];
	int ret;
	int value = 0;
	

	bcm2835_i2c_begin();
	bcm2835_i2c_setSlaveAddress(MPU6050_ADDRESS);

	regaddr[0]=REG_H;
	ret = BCM2835_I2C_REASON_ERROR_DATA;
	while(ret != BCM2835_I2C_REASON_OK)
	{
		//This is the basic operation to read an register
		//regaddr[0] is the register address
		//buf[0] is the value
		bcm2835_i2c_write(regaddr, 1);
		ret = bcm2835_i2c_read(buf, 1);
		//printf("%d\n",ret);
	}
	value = buf[0]<<8;
	regaddr[0]=(REG_H+1);
	
    ret = BCM2835_I2C_REASON_ERROR_DATA;
    while(ret != BCM2835_I2C_REASON_OK)
	{
		bcm2835_i2c_write(regaddr, 1);
		ret = bcm2835_i2c_read(buf, 1);
	}
	value += buf[0];
	
	if (value & 1<<15)
   	{
        	value -= 1<<16;
   	}
	bcm2835_i2c_end();
	//printf("%d ",value);
	return value;


}

void IMU::ComplementaryFilter()
{
	float pitchAcc, rollAcc;
	ReadGyr();
	ReadAccel();

	CFData.x += (GData.x/GYROSCOPE_SENSITIVITY)*dt;
	CFData.y -= (GData.y/GYROSCOPE_SENSITIVITY)*dt;
	//printf("CE: %f %f		",CFData.x,CFData.y);
	int forceMagnitudeApprox = abs(AData.x) + abs(AData.y) + abs(AData.z);
	if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768)
    	{
		// Turning around the X axis results in a vector on the Y-axis
		pitchAcc = atan2f(AData.y,AData.z) * 180 / M_PI;
		CFData.x = CFData.x * 0.98 + pitchAcc * 0.02;
	 
		// Turning around the Y axis results in a vector on the X-axis
		rollAcc = atan2f(AData.x,AData.z) * 180 / M_PI;
		CFData.y = CFData.y * 0.98 + rollAcc * 0.02;
   	 }
	printf("PR: %f %f		",pitchAcc,rollAcc);
	printf("CU: %f %f\n",CFData.x,CFData.y);
	//printf("%f %f %f\n",GData.x,AData.x,CFData.x);

}




int WriteRegister(int REG,int value)
{

	regaddr[0]=REG;
	regaddr[1]=value;
	return(bcm2835_i2c_write(regaddr, 2));
}


void Timer::StartCycle()
{
	gettimeofday(&t1, NULL);
}


int Timer::WaitMs(int ms)
{
	elapsedTime=0;
	int i=0;
	while(elapsedTime < ms)
        {
            gettimeofday(&t2, NULL);
            elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
            elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
            usleep(5);
	    	i++;
            //wait(t1);
        }
	if(i==1)//it means it had just 1 round in 1 loop, so the cycle was too long
	{
		//printf("ERROR: cycle was too long, time:  %d\n, elapsedTime");
		return 1;
	}
	else
		return 0;

}













